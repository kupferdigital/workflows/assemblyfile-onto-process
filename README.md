# assemblyfile-onto-process

This repo does only contain an assembly file which is used in the deployment process to set what data is in the triple store.

It will be changed automatically from the CI when new data is added.

Acts as a single point of truth.
